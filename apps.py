# !/usr/bin/env python3
# _*_ coding:utf-8 _*_
"""
@File     : apps.py.py
@Project  : flaskApplication
@Time     : 2022/2/9 6:39 下午
@Author   : Liu hang
@Contact_1: 252326397@qq.com
@Contact_2: 
@Software : PyCharm
@License  : (C)Copyright 2021-2028, 
@Last Modify Time      @Version     @Desciption
--------------------       --------        -----------
2022/2/9 6:39 下午        1.0             None
"""

from flask import Flask,request
from gevent.pywsgi import WSGIServer
from werkzeug.exceptions import HTTPException
from application1.a1 import application1_blueprint
from application2.a2 import application2_blueprint

app = Flask(__name__)


@app.errorhandler(HTTPException)
def handle_exception(e):
    return "ok"
    # return e.description


@app.errorhandler(404)
def not_found(error):
    return "not available %s" % request.base_url


app.register_blueprint(application1_blueprint)
app.register_blueprint(application2_blueprint)

http_server = WSGIServer(('', 5000), app)
http_server.serve_forever()
