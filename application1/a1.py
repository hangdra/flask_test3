# !/usr/bin/env python3
# _*_ coding:utf-8 _*_
"""
@File     : a1.py
@Project  : flaskApplication
@Time     : 2022/2/10 3:19 下午
@Author   : Liu hang
@Contact_1: 252326397@qq.com
@Contact_2: 
@Software : PyCharm
@License  : (C)Copyright 2021-2028, 
@Last Modify Time      @Version     @Desciption
--------------------       --------        -----------
2022/2/10 3:19 下午        1.0             None
"""

from flask import Blueprint

application1_blueprint = Blueprint('a1', __name__)


# print('a1 {%s}'% __name__)
@application1_blueprint.route('/app1')
def application1_index():
    return 'Home route for application1'


@application1_blueprint.route('/hello1')
def hello1():
    return 'Hello World from application1!'
